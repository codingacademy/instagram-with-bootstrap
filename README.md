#The Super Photo Feed

## Instructions

1. Create a website that looks exactly [like this one](https://projects.breatheco.de/p/css/junior/bootstrap/instagram-feed-bootstrap/preview.gif).
2. All the layout needs to be done using the bootstrap grid system.
3. The projects has to be 100% responsive.
4. Use the bootstrap components if needed.

### Resources

You have the online lesson about bootstrap and the cheat sheet about bootstrap in the assets section. We also recommend Net Ninja's tutorials on bootstrap.

### What to do if you are stuck?

- Clean the cache, using the incognito mode on Google Chrome. 
- Look in google for solutions. 
- Talk to the other students. 
- Talk to senior developers you know. 
- Talk to the instructors over slack.

### What **NOT** to do if you are stuck?

- Don't get frust rated and think you are not good at this: All the developers get stuck all the time, ask around! How do you know if you are good at something that you don't know? 
Coding is a science, facts matter and you have no facts. 
- Don't Keep wondering around for hours without seeking help: Google is only as useful as you are good at googling, please speak to other students or your instructors. 
- Change strategy: Einstein once said, if you do the same stuff you will get the same results.